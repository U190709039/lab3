import javax.lang.model.util.ElementScanner14;
public class FindPrimes {

	public static void main(String[] args) {
		int number = Integer.parseInt(args[0]);
		int check;
		int cond;
		int sum;
		
		if (number<=1)
		{
		System.out.println("There is not a such a prime number !");	
		}
		
		else 
		{
			for (check=2;check<=number;check++) 
			{
				sum=0;
				for (cond = 2;cond<check;cond++)
				{ 
					if (check % cond == 0) 
					{
						sum++;
					}
						
				}
				if (sum == 0) 
				{
				System.out.print(","+ check);	
				}
			}
		}
		
		
	}

}
