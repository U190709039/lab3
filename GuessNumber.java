import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		int counter = 0;
		boolean cond = true;
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
		int guess = reader.nextInt(); //Read the user input;

		while (cond== true)
		{
			if ( guess == -1)
			{
				counter++;
				System.out.println("Sorry,the number was " + number);
				cond = false;
				break;
			}

			else if (guess == number)
			{	cond = false;
				counter++;
				System.out.println("Congratulations! You won after " + counter + " attemps!");
				reader.close(); //Close the resource before exiting
			}
			else if ( guess > number)
			{
				counter++;
				System.out.println("Sorry!");
				System.out.println("Mine is less than your guess.");
				System.out.print("Type -1 to quit or guess another:");
				guess = reader.nextInt(); //Read the user input;


			}
			else if (guess < number)
			{
				counter++;
				System.out.println("Sorry!");
				System.out.println("Mine is greater than your guess.");
				System.out.print("Type -1 to quit or guess another:");
				guess = reader.nextInt(); //Read the user input;


			}


		}




	}


}